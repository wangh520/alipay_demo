package zfb.demo.listen;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import zfb.demo.util.AliPayUtils;

import java.io.IOException;
import java.util.Timer;

/**
 * @Author: wangh
 * @Date: 2019/12/21 0021 17:23
 * @Project demo
 * @extend： 主动向支付宝查询，交易情况
 */
@Component
public class PayConsumerMQ {


    /**
     * RabbitMQ 队列监听会被调用，并传来订单id
     *
     * @param orderId 商品订单id
     */
    @RabbitListener(queuesToDeclare = @Queue(name = "PayStatusQueryQueue"))
    public void getExpirePay(String orderId,Message message, Channel channel) throws AlipayApiException, IOException {
        System.out.println("获取到ID：" + orderId + " 的商品，该订单有效支付时间以到，即将激活主动查询支付宝支付结果！");
        AlipayClient aliPayClient = AliPayUtils.getAliPayClient();
        AlipayTradeQueryRequest queryRequest = new AlipayTradeQueryRequest();
        // 填充订单数据
        AlipayTradeQueryModel queryModel = new AlipayTradeQueryModel();
        // 设置订单编号来查询
        queryModel.setOutTradeNo(orderId);

        // 放入查询请求中
        queryRequest.setBizModel(queryModel);
        AlipayTradeQueryResponse queryResponse = aliPayClient.execute(queryRequest);
        if (queryResponse.isSuccess()) {
            // 请求成功
            System.out.println("支付宝交易号       : " + queryResponse.getTradeNo());
            System.out.println("商家订单号         : " + queryResponse.getOutTradeNo());
            System.out.println("买家支付宝账号     : " + queryResponse.getBuyerLogonId());
            System.out.print("交易状态:");
            switch (queryResponse.getTradeStatus()) {
                case "WAIT_BUYER_PAY":
                    System.out.println("交易创建，等待买家付款");
                    break;
                case "TRADE_CLOSED":
                    System.out.println("未付款交易超时关闭，或支付完成后全额退款");
                    break;
                case "TRADE_FINISHED":
                    System.out.println("交易结束，不可退款");
                    break;
                case "TRADE_SUCCESS":
                    System.out.println("交易支付成功");
                    break;
            }

            System.out.println("交易的订单金额     : " + queryResponse.getTotalAmount());
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
        } else {
            System.err.println("延时主动向支付宝查询失败！");
            //参数为：消息的DeliveryTag，是否批量拒绝，是否重新入队
            try {
                Thread.sleep(5 * 1000); //设置暂停的时间 5 秒
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
        }
    }

}
