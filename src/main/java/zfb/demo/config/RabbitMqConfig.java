package zfb.demo.config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @Author: wangh
 * @Date: 2019/12/21 0021 16:09
 * @Project demo
 * @extend： 使用两个交换机和队列来实现延迟队列
 */
@Configuration
public class RabbitMqConfig {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    //支付宝付款交换机
    @Bean
    public TopicExchange aliPayPayExchange(){
        return new TopicExchange("AliPayPayExchange",true,false);
    }

     // 付款状态查询交换机
    @Bean
    public TopicExchange payStatusQueryExchange(){
        return new TopicExchange("PayStatusQueryExchange",true,false);
    }

    // 延时队列
    @Bean
    public Queue aliPayPayQueue(){
        HashMap<String, Object> map = new HashMap<>();
        // 设置延迟时间（毫秒）
        map.put("x-message-ttl",1*10*1000);

        // x-dead-letter-routing-key 声明了这些死信在转发时携带的 routing-key
        map.put("x-dead-letter-routing-key", "ailpay.order.status.query");

        //绑定过期死信交换机
        map.put("x-dead-letter-exchange","PayStatusQueryExchange");
        return new Queue("AliPayPayQueue",true,false,false,map);
    }

    //付款状态查询队列
    @Bean
    public Queue payStatusQueryQueue(){
        return new Queue("PayStatusQueryQueue",true,false,false);
    }

    // 交换机绑定
    @Bean
    public Binding aliPayPayBinding(){
        return BindingBuilder.bind(aliPayPayQueue()).to(aliPayPayExchange()).with("submit.ailpay.order");
    }

    // 交换机绑定
    @Bean
    public Binding payStatusQueryBinding(){
        return BindingBuilder.bind(payStatusQueryQueue()).to(payStatusQueryExchange()).with("ailpay.order.status.query");
    }


}

@Component
class RabbitTemplateConfig implements RabbitTemplate.ConfirmCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    public void init() {
        rabbitTemplate.setConfirmCallback(this);            //指定 ConfirmCallback
    }

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        System.out.println("消息唯一标识：" + correlationData);
        System.out.println("确认结果：" + ack);
        System.out.println("失败原因：" + cause);
    }
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("消息主体 message : "+message);
        System.out.println("消息主体 message : "+replyCode);
        System.out.println("描述："+replyText);
        System.out.println("消息使用的交换器 exchange : "+exchange);
        System.out.println("消息使用的路由键 routing : "+routingKey);
    }

}
