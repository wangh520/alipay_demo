package zfb.demo.util;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

/**
 * @Author: wangh
 * @Date: 2019/12/17 0017 18:55
 * @Project demo
 * @extend： 支付宝工具类（用于初始化一个（单例）支付宝支付请求对象）
 */
public class AliPayUtils {
    /*// 你自己的私钥
    private static final String APP_PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDc/M7fdJr7sZanJWLAp4NhV1lkJAvO9Kpyi/BaQox+RqqXvAaQjWhbjyoK2H0wEGe7fWJiNw2ClswhLDSK4KmVhEND5xoN6Bw+c5CAC54DHhMwtDyTrqVo0sZvUZs+adA8QAw2uzivWP03zBLEwmp1veNjXtRuG/Ysqld4amwwbBUL9kcIMYOhT1GZHEN3YmyYVIk171W1peoj315KA6FMjEcJqD32wUbmpwANVacHASSWFAQlJI4PHWaCewm8lZTXYIvVQlF0ows0hNGHQkoCM2/TX6ZPpwzVmVkq4tieX7HKpvoUlurNpulKF/jtcO4qNNF4qWs33YiEzgsk0q37AgMBAAECggEBAKawYvMWp5jW2cZ2YsjjEiyokJsCvY17692/RKswq3zMnP0bLPB4WZFGqfXIWpWYqkY4S6+9ywkH6i3ThEaFNCW9wGdFuNdoj3VkAkkzehrr4pnIOjFDeS1aX6e0LJ+lfRcVUKEwE4/gVhBydG3KIDdz5kMcUqxDlysVyijgTX8vgEg2qRrpTDBflEm97edkjTRVuZ7yY1NWQEDEB+DuQgpThrNpeVQkv8O3HUSXaVvhK5Jk07lVed82yeJ2QJh6WNEC3SqQ8MdrbH6J6tWtZUP7h596UpO9KPnr+O0UIlrNiW2u2cCTcQQfHkoe7e17hXLFBh7i7f9fElsPIpy0DdECgYEA+3E/Dohsz2DATY8Yh/OprxZlTd241LUV2DtTPxNMpyTbMoIZZ+TwfvIVZXwkzIWSu0bdypUbM0PqtMh3K8uYqu4VwqtmOle9K8mon3LZLN2cf/Sg6A45GSZnLA0ALLgq8Aa6WDdZYovQUoY1QGFlQ1RNTHQmS2IGXXyGxAIzpgcCgYEA4P4+csq4vwPbJgEIoh9BBzSCYJ3oIRXOw7S33hpluDUbSdVZrdLFRk+yliVo6uT41rnBNUYe/BViL9ceRb+MRaDFubFXn+DgbN3Sy2PG53bs8hIrSWzxD3+Rh+8YpWEy7Ca11cRwXiLzxgTUYDflVJGN8g8kxpCOK/Fu2CCL220CgYEAislxcsYeuUDAJI9NjQ0Tpg8Ij7hIoy/pBNeKFZDDiKaxTum8uDoH/SoGYWPqs/EukqyFE6LFxsBANtVH4iaY+BA+dptA7Act5ZODva8Y7MjN71rJzgy7u4QVAp2ScRyepqowBS1X6ysdK8/kaMrZqBZKLF8YHBfZR5TKljpY49UCgYEAiNeZu8+4ldUOkQrH6XMA5hT1xzXDXXwaCcX8nsWHYIZMVpfAt4weGCmZAOE38sXY4yCPdotzhPashVRdQSbDdgXWvJ40ITVwykYQU0wYSed3LbQHR91OsJJyPS8a2ltH0YGpzp5viZHQmnuNgAf/H3R31OWJBIwfLHLna+Q/q/ECgYA18UiwKnJx1mE8YNSqUkD0GT7Z91EHp2Ocj9ePu0DdcdjwTy4tEZT5+3awpIYXYMscwMTSb4hNADpJGukjwpa3T5Pv5ATZWIfOmCbbUMBqfpUP7IvZXUSDaBWsIWVI3FnGtP9ijKqO5CSmGNmlj/aUbg60toMHsiK8QdYBGnaR5A==";
    // 支付宝公钥
    private static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoQoqj8qNF3fzazmDFuyDADN/Edq5Rf9fNmkuBf+JEEJBmTH6X66juNpJ7fPcpdn1k2K4gzEKR+dePvgmerrofoWkTOaE6lvHrPGuTirczSxs/e1wPXVjXIFWHAR/z/grNYV9nolDf+n9gF6GJxi50ezUJxK2npYkzJTWxYxJvb5fb507UMA6Wnz4XAxQ3n3KaIjrBCIcwWW76eyCQkFp8IgOhgjLf5zPf0hO78uI3P6wQC1SHQWNxsJt1jY/Y9h/6QWFEtT5XQs+Lx5H9aMIMiA4Ujl9kW7TxXYhW/S7rlfWWCKaIlLUVNEeGAo9OjO1PLMJCpqHPyREL0tjBHrc0QIDAQAB";
    // 应用ID
    private static final String APP_ID = "2021000119632861";*/
    // 你自己的私钥
    private static final String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCTb3tUVrxYSmYj42N+qWbu4XvkqHtsBGjGNUhWS4SAEoSMt6y9jhJHWg1rM62bQc2Mv6aATax3EfWjWPr1lkEr0C18LOF9cXBADqJJG0rko9GXNtKsmhpdiFvvAdUBJVq5TwXSkMs8L3UEM4Gr/rMT8oSZIJvVM81xM1jlpTfFkbLZ5wGIgbuK0xRR1RyAEfCmVx8v5a/jC4LEJsEmFYApN1sq94G96PR1DWW+FKokszqixTTUe0MidFb+C8bQKVv18w+ydbBGsR9Q9Y7x0Zb5+33xuy95ZKXl96ZyzsbmjkE0GYMLxSs2Gl10tbuEL5v6wd3puw12czLYQWb7lWUxAgMBAAECggEAFib++doabI2pMSU+Us6zN5tLmTyzZrR25akl2El6fB/MKLSEUOri1jOypnPaVuSZNNeq9cY17qIVLVj2RCYyXpnl6612p/l+azGHssJWC2qN2FGVb/erlHgjkBLPZV+udP9pmYzF7KIYy+vNRQF8+LdMsxFSOMOSz7puVfrwGVAjXWQ5xcEVtCGb3iZws41DEAHmgjzlalGqiiW2sQw0o1lBT8fVvIU0kT5aNAbhG0rU55in0RvaLkrlYeeNiHQNJYd/U6L0hM3T7wSJ9bOKbIewe2sb7f2/gz1Nf4q0l3ocaBY5x1FZRGM4nOdz/4y3z7tHH7ksbcdYjhlEpbWmkQKBgQDTuSp+G+08dBEseIs/1gqSt9lOSsuTFUSn77gKtFCjAWSLgYqn5NznuLp8O5mjVhK0Fg9TyIxa+3bVD7jj36N2xU5LKjJdoQ0hkXPL8iNoH88LzYAJnn0YsZD8Eb9l8yvbyO1RMu+HSBMLDxghoe8uxtbpHE2SPxOpQRN9G11rXQKBgQCyRJkS+/zJnm6BPer5VwTURDUCQtbkLNKOtpSYeaYkh/CjstvLxhihYRLk30pa9Rr4TgPp5FjkZ6ULrBvxG7CsCCX699aLTWvRHSofs1aD6kNmzb9CbI1ne3Ut2pgmRN58jS/xH9tsMovHzLHvFBIde2IiPxuwOhvbSjsF5usX5QKBgBNNg3FB7IwhbNtoFhH42rfrqRU5PnwnaT4CXf4j2GI0I02Qvup1tCowLlhbOrOoikX+6ODTnPBCkt/z8gBciFOww2WY8ESXBVJ1J2CFr/KzS7YgwzadS3xv2+8PUyrhU7NTC0fm4+2qhYnxk1qln9/c4wUXvn7B33ID69nxH8BFAoGBAIftPFa4YwiM0C+luawDSQBdPZoKwvXao3YEC4uzfhZuvPLk5ykaVB+A+Dohc/2k1LquKdfGsG0ABl2/DtMqTOzvjZ4uKZ2wG9yoAz2GrhoJnvkdOo94W92iyjd7bPYJHygTM5IEu3dllyVyk937DNEEClCYlBEsTZ9Im3alm25tAoGBAJu0M/FKoa0x6MgIRZlEnpuJ7VqOE/IlK9LxtbAdSTF/MXmLaMeWAbJnni/sdPDx/9/0yGXIhIYtyWB6hGv8/H0eX0r0k7eKRSxEqQOdMgC36w1WWMp1qMQnPPamamZscovicO97CFVdXbzbdNp6FlBOm8f5ksl8l8MuMDk6kH3i";
    // 支付宝公钥
    private static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuaRSjo47PfVauvh7vBW4msttZ9LWhbOHoSo+2KVII5yJQ//S6jYoPEDLwo9Wl2szsu3trTd6Zkr1mkGcL48CooJPCMROrj6WxCwyS7wm1QmiAHbbCauq3UaS4tXrYP7086b7MI4CtRu8P4QFXoyBJpjiB3OzMF2dKSFUnQ8PjqpiZNUQhnGxITbrG2uGWT4FJiHe1xB657qRQGoevzvZAD8v0ex0hLyxv1aTHTS8D7FuuBn17sy3+HHEp+k3bue0OH5WLzyDGB5OzKLL2uhBIqCQ7Eh5fakLcMfV2HFFMErOWSpNrSA8ipYo/RvLlckufTAE+WZUIXJyRabcUurRhwIDAQAB";
    // 应用ID
    private static final String APP_ID = "2021003123643050";
    private static AlipayClient alipayClient;


    static {
        //创建一个支付宝客户端，单例即可
        alipayClient = new DefaultAlipayClient(
                //支付宝网关（真正的和沙箱的不一样）
//                "https://openapi.alipaydev.com/gateway.do",
                "https://openapi.alipay.com/gateway.do",
                //创建的应用ID（类似oAuto2协议）
                APP_ID,
                //开发者加密数据的密钥
                APP_PRIVATE_KEY,
                //传输数据的格式
                "JSON",
                //字符集
                "UTF-8",
                //支付宝的公钥
                ALIPAY_PUBLIC_KEY,
                //签名方式
                "RSA2"
        );

    }

    public static  AlipayClient getAliPayClient(){
        return alipayClient;
    }

}
