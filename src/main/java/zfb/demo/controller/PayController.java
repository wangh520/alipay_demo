package zfb.demo.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeCloseModel;
import com.alipay.api.domain.AlipayTradePagePayModel;
import com.alipay.api.domain.ExtendParams;
import com.alipay.api.request.AlipayTradePagePayRequest;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import zfb.demo.util.AliPayUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @Author: wangh
 * @Date: 2019/12/17 0017 18:50
 * @Project demo
 * @extend：  调用支付宝接口，请求html支付页面 （应该写在service层）
 */
@Controller
@RequestMapping("/pay")
public class PayController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping("/alipay")
    public void payPage(HttpServletResponse response,String price,String subject) throws IOException {
        String uuid = UUID.randomUUID().toString();
        double prices = BigDecimal.valueOf(Double.parseDouble(price)).doubleValue();
        //获得支付宝的客户端实例
        AlipayClient aliPayClient = AliPayUtils.getAliPayClient();
        //创建API对应的request
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        //支付成功和支付失败的回调页面（即执行支付操作完成后跳转的页面）
//        alipayRequest.setReturnUrl("http://127.0.0.1:8088/");
        alipayRequest.setReturnUrl("http://wh.rfd.nmzjhz.com/");
        //异步回调支付的结果（公网的URL）
        alipayRequest.setNotifyUrl("");

        // 填充业务参数的model
        AlipayTradePagePayModel payModel = new AlipayTradePagePayModel();
        // 订单号（不可重复）
        payModel.setOutTradeNo(uuid);
        // 价格
        payModel.setTotalAmount(price);
        // 设置该订单支付的有效时间（如果一分钟内没有支付该订单则失效） 1m～15d范围，m-分钟，h-小时，d-天，1c-当天。该参数数值不接受小数点， 如 1.5h，可转换为 90m
        payModel.setTimeoutExpress("1m");
        // 支付单的标题
        payModel.setSubject(subject);
        // 订单描述
        payModel.setBody("你抢购的商品是："+uuid+"=="+subject+"如未支付订单将在1分钟内失效！");
        // 销售码默认FAST_INSTANT_TRADE_PAY
        payModel.setProductCode("FAST_INSTANT_TRADE_PAY");
        // 填充数据到请求提体
        alipayRequest.setBizModel(payModel);

        //返回的HTML页面
        String form="";
        try {
            form = aliPayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();

        //商品存入延迟消息退队列，做主动查询支付结果（以防支付宝异步支付回调失败）
        rabbitTemplate.convertAndSend("AliPayPayExchange","submit.ailpay.order",uuid);


    }


}
