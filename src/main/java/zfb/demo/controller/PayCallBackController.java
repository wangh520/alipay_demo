package zfb.demo.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import org.hibernate.validator.constraints.URL;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * @Author: wangh
 * @Date: 2019/12/17 0017 18:52
 * @Project demo
 * @extend：
 */
@Controller
@RequestMapping("/callback")
public class PayCallBackController {
    //测试版公钥
//    private static final String ALIPAY_PUBLIC_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDc/M7fdJr7sZanJWLAp4NhV1lkJAvO9Kpyi/BaQox+RqqXvAaQjWhbjyoK2H0wEGe7fWJiNw2ClswhLDSK4KmVhEND5xoN6Bw+c5CAC54DHhMwtDyTrqVo0sZvUZs+adA8QAw2uzivWP03zBLEwmp1veNjXtRuG/Ysqld4amwwbBUL9kcIMYOhT1GZHEN3YmyYVIk171W1peoj315KA6FMjEcJqD32wUbmpwANVacHASSWFAQlJI4PHWaCewm8lZTXYIvVQlF0ows0hNGHQkoCM2/TX6ZPpwzVmVkq4tieX7HKpvoUlurNpulKF/jtcO4qNNF4qWs33YiEzgsk0q37AgMBAAECggEBAKawYvMWp5jW2cZ2YsjjEiyokJsCvY17692/RKswq3zMnP0bLPB4WZFGqfXIWpWYqkY4S6+9ywkH6i3ThEaFNCW9wGdFuNdoj3VkAkkzehrr4pnIOjFDeS1aX6e0LJ+lfRcVUKEwE4/gVhBydG3KIDdz5kMcUqxDlysVyijgTX8vgEg2qRrpTDBflEm97edkjTRVuZ7yY1NWQEDEB+DuQgpThrNpeVQkv8O3HUSXaVvhK5Jk07lVed82yeJ2QJh6WNEC3SqQ8MdrbH6J6tWtZUP7h596UpO9KPnr+O0UIlrNiW2u2cCTcQQfHkoe7e17hXLFBh7i7f9fElsPIpy0DdECgYEA+3E/Dohsz2DATY8Yh/OprxZlTd241LUV2DtTPxNMpyTbMoIZZ+TwfvIVZXwkzIWSu0bdypUbM0PqtMh3K8uYqu4VwqtmOle9K8mon3LZLN2cf/Sg6A45GSZnLA0ALLgq8Aa6WDdZYovQUoY1QGFlQ1RNTHQmS2IGXXyGxAIzpgcCgYEA4P4+csq4vwPbJgEIoh9BBzSCYJ3oIRXOw7S33hpluDUbSdVZrdLFRk+yliVo6uT41rnBNUYe/BViL9ceRb+MRaDFubFXn+DgbN3Sy2PG53bs8hIrSWzxD3+Rh+8YpWEy7Ca11cRwXiLzxgTUYDflVJGN8g8kxpCOK/Fu2CCL220CgYEAislxcsYeuUDAJI9NjQ0Tpg8Ij7hIoy/pBNeKFZDDiKaxTum8uDoH/SoGYWPqs/EukqyFE6LFxsBANtVH4iaY+BA+dptA7Act5ZODva8Y7MjN71rJzgy7u4QVAp2ScRyepqowBS1X6ysdK8/kaMrZqBZKLF8YHBfZR5TKljpY49UCgYEAiNeZu8+4ldUOkQrH6XMA5hT1xzXDXXwaCcX8nsWHYIZMVpfAt4weGCmZAOE38sXY4yCPdotzhPashVRdQSbDdgXWvJ40ITVwykYQU0wYSed3LbQHR91OsJJyPS8a2ltH0YGpzp5viZHQmnuNgAf/H3R31OWJBIwfLHLna+Q/q/ECgYA18UiwKnJx1mE8YNSqUkD0GT7Z91EHp2Ocj9ePu0DdcdjwTy4tEZT5+3awpIYXYMscwMTSb4hNADpJGukjwpa3T5Pv5ATZWIfOmCbbUMBqfpUP7IvZXUSDaBWsIWVI3FnGtP9ijKqO5CSmGNmlj/aUbg60toMHsiK8QdYBGnaR5A==";
    //你自己的公钥
    private static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk297VFa8WEpmI+Njfqlm7uF75Kh7bARoxjVIVkuEgBKEjLesvY4SR1oNazOtm0HNjL+mgE2sdxH1o1j69ZZBK9AtfCzhfXFwQA6iSRtK5KPRlzbSrJoaXYhb7wHVASVauU8F0pDLPC91BDOBq/6zE/KEmSCb1TPNcTNY5aU3xZGy2ecBiIG7itMUUdUcgBHwplcfL+Wv4wuCxCbBJhWAKTdbKveBvej0dQ1lvhSqJLM6osU01HtDInRW/gvG0Clb9fMPsnWwRrEfUPWO8dGW+ft98bsveWSl5femcs7G5o5BNBmDC8UrNhpddLW7hC+b+sHd6bsNdnMy2EFm+5VlMQIDAQAB";

    @PostMapping("/alipay")
    @ResponseBody
    public boolean aliPayCallBack(HttpServletRequest request) throws AlipayApiException, UnsupportedEncodingException {
        //获取支付宝POST过来反馈信息
        Map<String, String> params = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        parameterMap.forEach((key, value) -> params.put(key, value[0]));
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPAY_PUBLIC_KEY, "UTF-8", "RSA2"); //调用SDK验证签名
        if (signVerified) {//验证成功
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");

            if (trade_status.equals("TRADE_FINISHED")) {
            } else if (trade_status.equals("TRADE_SUCCESS")) {
            }
            System.out.println("支付成功");

        } else {//验证失败
            System.out.println("支付失败");
        }

        return true;
    }
}
